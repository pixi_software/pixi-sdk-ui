<?php

namespace Pixi\Ui\Table;

/**
 * Table column element instance.
 *
 * @author pixi
 *        
 */
class TableColumn
{

    /**
     *
     * @var <string> : Name of the table column field.
     */
    public $FieldName;

    /**
     *
     * @var <string> : Title of the table column.
     */
    public $Title;

    /**
     *
     * @var <cosnt> : Table column values format.
     */
    public $Format;

    /**
     *
     * @var <string> : Color of the table column.
     */
    public $Color;

    /**
     *
     * @param <mixed> $mapping
     *            : If array then values is mapped. Array keys are searched againt table value and if found
     *            array values is replaced with original value.
     */
    public $mapping;

    /**
     * Creates new table column element.
     * 
     * @param <string> $FieldName
     *            : Name of the table column field.
     * @param <string> $Title
     *            : Table column values format.
     * @param <const> $Format
     *            : Table column values format.
     * @param <string> $Color
     *            : Color of the table column.
     * @param <mixed> $mapping
     *            : If array then values is mapped. Array keys are searched againt table value and if found
     *            array values is replaced with original value.
     */
    function __construct($FieldName, $Title, $Format = DataFormat::FORMAT_STRING, $Color = NULL, $mapping = NULL, $Edit = NULL)
    {
        $this->Title = $Title;
        $this->FieldName = $FieldName;
        $this->Format = $Format;
        $this->Color = $Color;
        $this->mapping = $mapping; //tolower? why?
        //For Editable
        $this->Edit = $Edit;
    }

    public function isEditable()
    {
        if (!empty($this->Edit)) {
            return true;
        } else {
            return false;
        }
    }

}
