<?php

/**
 * Table HTML element. 
 * 
 * @author gwalter
 *
 */

namespace Pixi\Ui\Table;

use Pixi\Ui\Data\DataFormat;
use Pixi\Ui\Data\DataContainer;

class Table extends DataContainer
{

    /**
     *
     * @var <mixed> : Table columns storage.
     */
    protected $Columns = [];

    /**
     *
     * @var <mixed> : Table rows storage;
     */
    protected $Rows = [];

    /**
     *
     * @var <string> : Table name.
     */
    public $Title;

    /**
     *
     * @var <const> : Table type.
     */
    protected $Type;

    /**
     *
     * @var <string> : Table html id property.
     */
    public $ID;

    /**
     *
     * @var <int> : Makes a new TR after this nr of TDs.
     */
    public $lineBreak = 12;

    /**
     *
     * @var <mixed> : Set the Link to delete an item form a list.
     */
    protected $deleteURL;

    /**
     *
     * @var <mixed> : Delete table column html id.
     */
    protected $deleteColumnID;

    /**
     *
     * @var <string> : Set the Link to delete an item form a list.
     */
    protected $editURL;

    /**
     *
     * @var <string> : Set the Link to an editable ajax event
     */
    protected $editableUrl;

    /**
     *
     * @var <string> : Set the Link to a Data source
     */
    protected $editableSource;

    /**
     *
     * @var <string> : Set the primary Key field name
     */
    protected $primaryKey;

    /**
     *
     * @var <string> : Edit table column html ID.
     */
    protected $editColumnID;

    /**
     *
     * @var <string> : Set the Link to delete an item form a list.
     */
    protected $detailURL;

    /**
     *
     * @var <sting> : Details column html ID property.
     */
    protected $detailColumnID;

    /**
     *
     * @var <string> : Detaul column detail html id property.
     */
    protected $detailColumnDetailID;

    /**
     *
     * @var <const> : Tells that table type would be plain (ordinary) table.
     */
    const TableTypePlain = 'plain';

    /**
     *
     * @var <const> : Tells that table type would be datatables.
     */
    const TableTypeDataTables = 'datatables';

    /**
     * Creates new HTML table instance.
     *
     *
     * @param <string> $Title
     *            : Title of the table.
     * @param <const> $Type
     *            : Tells the type od the table.
     */
    function __construct($Title, $Type = Table::TableTypeDataTables)
    {
        $this->Title = $Title;
        $this->Type = $Type;
        $this->rowsPerPage = 50;
    }

    /**
     * Add new column to the table.
     *
     * @param <string> $FieldName
     *            : Name of the field.s
     * @param <string> $Title
     *            : Title of the field.
     * @param <const> $Format
     *            : Format of the column value.
     * @param <string> $Color
     *            : Color of the table column.
     * @param <mixed> $mapping
     *            : If array then values is mapped. Array keys are searched againt table value and if found
     *            array values is replaced with original value.
     */
    public function addColumn($FieldName, $Title, $Format = 'string', $Color = NULL, $mapping = NULL, $Edit = NULL)
    {
        $this->Columns[] = new TableColumn($FieldName, $Title, $Format, $Color, $mapping, $Edit);
    }

    public function translateColumn($columnIdent)
    {
        $this->TranslateColumns[] = $columnIdent;
    }

    public function getColumnsToTranslate()
    {
        if (!empty($this->TranslateColumns)) {
            return $this->TranslateColumns;
        } else {
            return array();
        }
    }

    public function addLanguage($lang)
    {
        $this->lang = $lang;
    }

    /**
     * Adds new row to the table.
     *
     * @param <mixed> $data
     *            : Populate rows with data.
     */
    function addRows($data)
    {
        if (is_array($data)) {
            foreach ($data as $row) {
                $this->Rows[] = $row;
            }
        }
    }

    /**
     * Enable a delete button per Row.
     *
     * @param unknown $URL            
     * @param unknown $ColumnID            
     */
    function enableDeleteLink($URL, $ColumnID)
    {
        $this->deleteURL = $URL;
        $this->deleteColumnID = $ColumnID;
    }

    /**
     * Enable a Detail Link on a column
     *
     * @param string $URL            
     * @param string $ColumnID
     *            this is the ID column
     * @param string $ColumnDetailID
     *            This is the name of the column that will become the link
     */
    function enableDetailLink($URL, $ColumnID, $ColumnDetailID)
    {
        $this->detailURL = $URL;
        $this->detailColumnID = $ColumnID;
        $this->detailColumnDetailID = $ColumnDetailID;
    }

    function enableEditable($URL, $primaryKey, $source = null)
    {
        $this->editableUrl = $URL;
        $this->editableSource = $source;
        $this->primaryKey = $primaryKey;
    }

    /**
     * Enable a Edit Button for each row.
     *
     * @param unknown $URL            
     * @param unknown $ColumnID            
     */
    function enableEditLink($URL, $ColumnID)
    {
        $this->editURL = $URL;
        $this->editColumnID = $ColumnID;
    }

    /**
     * Generate the HTML for the table.
     * Conists of Header and script footer.
     *
     * @param unknown $ID            
     * @return string
     */
    function generateHTML($ID = 0)
    {
        $html = '<h2>' . $this->Title . '</h2>
			<table class="table table-bordered" id="table_' . $ID . '">
			<thead><tr>';

        $counter = 0;
        
        foreach ($this->Columns as $column) {
            $counter ++;
            $html .= '<th>' . $column->Title . '</th>';
            if ($counter == $this->lineBreak) {
                $html .= '</tr><tr>';
            };
        };

        if (isset($this->deleteURL) or isset($this->editURL)) {
            if (isset($this->deleteURL) && isset($this->editURL)) {
                $html .= '<th colspan="2" class="sorting_disabled" role="columnheader"></th>';
            } else {
                $html .= '<th class="sorting_disabled" role="columnheader"></th>';
            }
        }

        $html .= '</thead></tr>';
        $html .= '<tbody>';

        if (!empty($this->Rows)) {
            foreach ($this->Rows as $item) {
                $counter = 0;
                $html .= '<tr>';
                foreach ($this->Columns as $column) {
                    $counter ++;
                    $fieldName = $column->FieldName;

                    // make this part work with Array or Objects as columns.
                    unset($value);

                    if (isset($item->$fieldName))
                        $value = $item->$fieldName;
                    if (is_array($item)) {
                        if (isset($item[$fieldName]))
                            $value = $item[$fieldName];
                    };

                    $html .= '<td>';

                    if ($column->isEditable() && $column->Edit == 'select') {
                        
                        $booleanSwitcher = true;

                        $primaryKey = $this->primaryKey;
                        $html .= '<input id="' . $fieldName . '-' . $item->$primaryKey . '" ';
                        $html .= (!empty($value)) ? '" checked="checked"' : ' ';
                        $html .= ' type="checkbox" class="boolean-switch ace ace-switch ace-switch-1"><span class="lbl"></span>';
                        
                    } else {

                        // Is this column the detail column?
                        if (isset($this->editableUrl) && $column->isEditable()) {
                            
                            $html .= '<a class="editable" href="#"';
                            $html .= ' id="' . $fieldName . '-' . $item->$primaryKey . '"';
                            $html .= ' data-name="' . $fieldName . '"';
                            $html .= (!empty($column->Title)) ? ' data-original-title="' . $column->Title . '"' : ' ';
                            $html .= (!empty($column->Edit)) ? ' data-type="' . $column->Edit . '"' : ' data-type="text"';
                            $html .= (!empty($this->editableSource)) ? ' data-source="' . $this->editableSource . '"' : ' ';
                            $html .= (!empty($this->editableUrl)) ? ' data-url="' . $this->editableUrl . '"' : ' data-url="#"';
                            $html .= (!empty($this->primaryKey)) ? ' data-pk="{' . $this->primaryKey . ' : ' . $this->getItemData($item, $this->primaryKey) . '}"' : ' data-pk="{ id : 1 }"';
                            $html .= '>';
                            
                        } elseif ($this->detailColumnDetailID == $fieldName) {

                            $html .= '<a href="' . site_url($this->detailURL) . '/' . $this->getItemData($item, $this->detailColumnID) . '">';
                        
                        }

                        if (!empty($value)) {

                            $translateColumns = $this->getColumnsToTranslate();

                            if (in_array($fieldName, $translateColumns)) {
                                $html .= $this->lang->line($value);
                            } else {
                                $html .= DataFormat::format($value, $column->Format, $column->mapping);
                            }
                            
                        }

                        if (isset($this->editableUrl) && $column->isEditable()) {
                            $html .= '</a>';
                        } elseif ($this->detailColumnDetailID == $column) {
                            $html .= '</a>';
                        }
                        
                    }

                    $html .= '</td>';
                    
                    if ($counter == $this->lineBreak) {
                        // after X columns, make a 2nd line
                        $html .= '</tr><tr>';
                    };
                    
                };
                
                if (isset($this->deleteURL)) {
                    $html .= '<td>
                        <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                            <a class="red" href="' . site_url($this->deleteURL) . '/' . $this->getItemData($item, $this->deleteColumnID) . '" onclick="return confirm(\'Wirklich entfernen?\')">
                                <i class="ace-icon fa fa-trash-o bigger-130"></i>
                            </a>
                        </div></td>';
                };
                
                if (isset($this->editURL)) {
                    $html .= '<td>
                        <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                            <a class="red" href="' . site_url($this->editURL) . '/' . $this->getItemData($item, $this->editColumnID) . '"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
                        </div></td>';
                };
                
                $html .= '</tr>';
                
            }
            
        }
        
        $html .= '</tbody>';
        $html .= '</table>';

        $result['body'] = $html;

        if ($this->Type == Table::TableTypeDataTables) {

            $result['head'] = '<link rel="stylesheet" href="'. base_url() . '/vendor/datatables/datatables/media/css/dataTables.bootstrap.css" />';

            $result['script'] = '<script src="' . base_url() . '/vendor/datatables/datatables/media/js/jquery.dataTables.min.js"></script>';
            $result['script'] .= '<script src="' . base_url() . '/vendor/datatables/datatables/media/js/dataTables.bootstrap.min.js"></script>';
            $result['script'] .= "<script type=\"text/javascript\" charset=\"utf-8\">";
            $result['script'] .= "$(document).ready(function() {";
            $result['script'] .= "$('#table_" . $ID . "').dataTable(";
            if (!empty($this->rowsPerPage)) {
                $result['script'] .= "{\"iDisplayLength\": " . $this->rowsPerPage . "} ";
            }
            $result['script'] .= ");});</script>";

            if (!empty($this->editableUrl)) {
                $result['script'] .= "<script type=\"text/javascript\" charset=\"utf-8\">";
                $result['script'] .= "$('.editable').editable({ }); ";
                if (!empty($booleanSwitcher)) {
                    $result['script'] .= "$('body').on('change', '.boolean-switch', function() {
                        $.post( '" . $this->editableUrl . "', { id: $(this).attr('id'), boolean: $(this).prop('checked') } );
                    } ); ";
                }
                $result['script'] .= "</script>";
            }
        }

        return $result;
        
    }
    
    public function getItemData($item, $field)
    {
        
        if(is_object($item)) {
            return $item->$field;
        } elseif(is_array($item) AND isset($item[$field])) {
            return $item[$field];
        } else {
            return false;
        }
        
    }

}
