<?php

namespace Pixi\Ui\Info;

use Pixi\Ui\Data\DataContainer;

/**
 * Info HTML element widget.
 *
 * @author pixi
 */
class Info extends DataContainer
{
    /**
     * Generates the HTML markup for the Info widget.
     *
     * @return multitype:string : Returns HTML markup string.
     */
    public function generateHTML()
    {
        $html = '<h3>'.$this->Title.'</h3><infobox-container>';
        foreach ($this->Data as $Info) {
            $html .= '
                <div class="infobox infobox-'.$Info->Color.'">
                    <div class="infobox-icon">
            			<i class="ace-icon fa '.$Info->Icon.'"></i>
                    </div>
                    <div class="infobox-data">
            			<span class="infobox-data-number">'.$Info->Title.'</span>
    		        	<div class="infobox-content">'.$Info->SubTitle.'</div>
                </div>
            </div>';
        }

        $html .= '</infobox-container>';

        return array(
            'body' => $html,
        );
    }
}
