<?php

namespace Pixi\Ui\Info;

use Pixi\Ui\Data\DataElement;

/**
 *
 *
 * Info form element.
 * 
 * @author pixi
 *        
 */
class InfoElement extends DataElement
{

    /**
     *
     * @var <string> : Color of the info element.
     */
    public $Color;

    /**
     * Creates new info element.
     *
     * @param <string> $Title
     *            : Title of the info element.
     * @param <string> $SubTitle
     *            : Subtitle of the info element.
     * @param <string> $Icon
     *            : Icon of the info element.
     * @param <string> $Color
     *            : Color of the info element.
     * @param <string> $URL
     *            : URL of the info element.
     */
    function __construct($Title, $SubTitle, $Icon, $Color = 'orange', $URL = null)
    {
        parent::__construct($Title, $SubTitle, $Icon, $URL);
        $this->Color = $Color;
    }

}
