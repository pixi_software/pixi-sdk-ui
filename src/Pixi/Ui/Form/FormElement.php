<?php

namespace Pixi\Ui\Form;

/**
 * Form element.
 *
 * @author gwalter
 *
 */
class FormElement
{

    /**
     *
     * @var <string> : Name of the form element.
     */
    public $Name;

    /**
     *
     * @var <string> : Label of the html form element.
     */
    public $Label;

    /**
     *
     * @var <mixed> : Value of the form element.
     */
    public $Value;

    /**
     *
     * @var <const> : Type of the form element.
     */
    public $Type;

    /**
     *
     * @var <const> : Validation type for form element values.
     * See https://jqueryvalidation.org/documentation/ for possible values
     */
    public $Validation;

    /**
     *
     * @var <cosnt> : Element type id.
     */
    const ElementTypeID = 'ID';

    /**
     *
     * @var <const> : Element type database ID.
     */
    const ElementTypeDatabaseID = 'DatabaseID';

    /**
     *
     * @var <const> : Element type string.
     */
    const ElementTypeString = 'String';

    /**
     *
     * @var <const> : Element type textarea.
     */
    const ElementTypeTextArea = 'TextArea';

    /**
     *
     * @var <cosnt> : Element type number.
     */
    const ElementTypeNumber = 'Number';

    /**
     *
     * @var <const> : Element type read only.
     */
    const ElementTypeReadOnly = 'ReadOnly';

    /**
     *
     * @var <const> : Element type drop down.
     */
    const ElementTypeDropDown = 'DropDown';

    /**
     *
     * @var <const> : Element type money.
     */
    const ElementTypeMoney = 'Money';

    /**
     *
     * @var <const> : Element type country.
     */
    const ElementTypeCountry = 'Country';

    /**
     *
     * @var <const> : Element type hidden.
     */
    const ElementTypeHidden = 'hidden';

    /**
     *
     * @var <const> : Element type password.
     */
    const ElementTypePassword = 'password';

    /**
     *
     * @var <const> : Element type radio.
     */

    const ElementTypeRadio = 'radio';


    /**
     *
     * @var <const> : Element type date picker (jquery).
     */

    const ElementTypeDate = 'date';



    /**
     * Creates a new form Elemnt.
     *
     * @param <string> $Name
     *            : HTML name property of the form element.
     * @param <string> $Label
     *            : Label of the form element.
     * @param <const> $Type
     *            : Type of the form element.
     * @param <array> $Value
     *            : Value of the form element.s
     * @param string $Validation
     *            : Validation rule for form eleemnt.
     */
    function __construct($Name, $Type, $Label, $Value = NULL, $Validation = NULL)
    {
        $this->Name = $Name;
        $this->Label = $Label;
        $this->Value = $Value;
        $this->Type = $Type;
        $this->Validation = $Validation;
    }

}
