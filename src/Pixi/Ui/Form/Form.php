<?php

namespace Pixi\Ui\Form;

use Guzzle\Tests\Common\Cache\NullCacheAdapterTest;

/**
 *
 * @author gwalter
 *
 */
class Form
{

    private $URL;
    public  $Elements = array();
    private $Values;
    private $Method;
    private $saveBtn;

    /**
     */
    function __construct($URL, $Method = 'POST', $saveBtn = 'Speichern')
    {
        $this->URL = $URL;
        $this->Method = $Method;
        $this->saveBtn = $saveBtn;
    }

    function addElement($Name, $Type, $Label, $Value = NULL, $Validation = NULL)
    {
        $this->Elements[] = new FormElement($Name, $Type, $Label, $Value, $Validation);
    }

    function addValues($Values)
    {
        $this->Values = $Values;
    }

    function generateHTML($ID = null)
    {


        $html = '<form action="' . site_url($this->URL) . '" method="'.$this->Method.'" accept-charset="utf-8" class="form-horizontal" role="form" name="form_' . $ID . '" id="form_' . $ID . '">';

        $counter = 0;

        $script = '';

        foreach ($this->Elements as $Element) {

            $counter++;

            $FieldName = $Element->Name;

            if (isset($this->Values->$FieldName)) {
                $value = $this->Values->$FieldName;
            } elseif (is_string($Element->Value)) {
                $value = $Element->Value;
            } elseif (is_array($this->Values) AND isset($this->Values[$FieldName])) {
                $value = $this->Values[$FieldName];
            } else {
                $value = null;
            }

            if ($Element->Type == FormElement::ElementTypeCountry) {
                $Element->Value = array(
                    array('ID' => 'D', 'Name' => lang('germany')),
                    array('ID' => 'A', 'Name' => lang('austria')),
                    array('ID' => 'CH', 'Name' => lang('swiss')),
                    array('ID' => 'F', 'Name' => 'Frankreich'),
                    array('ID' => 'NL', 'Name' => 'Niederlande'),
                    array('ID' => 'CZ', 'Name' => 'Tschechise Rep.'),
                );
                $Element->Type = FormElement::ElementTypeDropDown;
            }

            if ($Element->Type != FormElement::ElementTypeID)
                $html .= '<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-' . $Element->Name . '">' . $Element->Label . '</label>
										<div class="col-sm-9">';

            switch ($Element->Type) {

                case FormElement::ElementTypeString:
                    $html .= '<input type="text" id="form-field-' . $Element->Name . '" name="' . $Element->Name . '" class="col-xs-10 col-sm-5" value="' . $value . '" '.$Element->Validation.'>';
                    break;

                case FormElement::ElementTypeDate:
                    $html .= '<input type="text" id="form-field-' . $Element->Name . '" name="' . $Element->Name . '" class="col-xs-5 col-sm-2" value="' . $value . '" '.$Element->Validation.'>';
                    $script .= '<script>
                    $(function() {
                        $("#form-field-' . $Element->Name . '" ).datepicker();
                    });</script>';
                    break;


                case FormElement::ElementTypeTextArea:
                    $html .= '<textarea id="form-field-' . $Element->Name . '" name="' . $Element->Name . '" class="col-xs-10 col-sm-5" '.$Element->Validation.'>' . $value . '</textarea>';
                    break;

                case FormElement::ElementTypePassword:
                    $html .= '<input type="password" id="form-field-' . $Element->Name . '" name="' . $Element->Name . '" class="col-xs-10 col-sm-5" value="' . $value . '" '.$Element->Validation.'>';
                    break;

                case FormElement::ElementTypeID:
                    $html .= '<input type="hidden" id="form-field-' . $Element->Name . '" name="' . $Element->Label . '" value="' . $value . '">';
                    break;

                case FormElement::ElementTypeHidden:
                    $html .= '<input type="hidden" id="form-field-' . $Element->Name . '" name="' . $Element->Name . '" value="' . $value . '">';
                    break;

                    case FormElement::ElementTypeRadio:
                        $html .= '<input type="radio" id="form-field-' . $Element->Name . '" name="' . $Element->Name . '" value="' . $value . '" '.$Element->Validation.'>';
                        break;

                case FormElement::ElementTypeNumber:
                case FormElement::ElementTypeMoney:
                    if(strlen($value) > 0) {
                        $value = number_format($value, 2, '.', '');
                    } else {
                        $value = '';
                    }
                    $html .= '<input type="text" id="form-field-' . $Element->Name . '" name="' . $Element->Name . '" class="col-xs-4 col-sm-2" value="' . $value . '" '.$Element->Validation.'>';
                    break;

                case FormElement::ElementTypeReadOnly:
                    $html .= '<input type="text" id="form-field-' . $Element->Name . '" name="' . $Element->Name . '" class="col-xs-10 col-sm-5" value="' . $value . '" disabled>';
                    break;

                case FormElement::ElementTypeDropDown:
                    //echo 'I am a dropdown - '.$Element->Name;
                    $html .= '<select id="form-field-' . $Element->Name . '" name="' . $Element->Name . '" '.$Element->Validation.'>';
                    foreach ($Element->Value as $ElementValue) {
                        //echo 'ID: '.$ElementValue['ID'];
                        //echo 'Value: '.$value;
                        $html .= '<option value="' . $ElementValue['ID'] . '"';
                        //echo 'Value: '.$value.' Element: '.$ElementValue['ID'];
                        if ($ElementValue['ID'] == trim($value))
                            $html .= ' SELECTED ';
                        $html .= '>' . $ElementValue['Name'] . '</option>';
                    };
                    $html .= '</select>';
                    break;

                    //format
            };

            if ($Element->Type != FormElement::ElementTypeID) {
                $html .= '</div></div>';
            }

        }

        /**
         * This is the submit button for the form...
         */
        if($this->saveBtn) {
            $html .= '<div class="clearfix form-actions">
                            <div class="col-md-offset-3">
                                <button type="submit" class="btn btn-primary" id="form-field-submit">
                                    <i class="icon-shopping-cart bigger-110"></i> <span>'.$this->saveBtn.'</span>
                                </button>
                            </div>
                        </div>';
        }

        /*
          <a href="javascript: submitform_'.$ID.'();"	class="btn btn-block btn-primary">
          <span>Speichern</span>
          </a>
          </div>

          <script type="text/javascript">
          function submitform_'.$ID.'()
          {
          document.form_'.$ID.'.submit();
          }
          </script>
          '; */

        $html .= '</form>';

        if(file_exists(base_url () . 'assets/pixi/js/jquery.validate.min.js')) {
            $script .=
                '<script src="' . base_url () . 'assets/pixi/js/jquery.validate.min.js"></script>
                <script>$("#form_' . $ID . '").validate();<script>';
        }

        //$id = $xyz;
        return array('body' => $html, 'script' => '', 'head' => '', 'script' => $script);
    }

}
