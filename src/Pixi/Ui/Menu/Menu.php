<?php

/**
 * Main Menu Class
 * Can be used for main menu, Log-Dropdown or adding items to the user dropdown.
 * @author gwalter
 *
 */

namespace Pixi\Ui\Menu;

class Menu
{

    public $menuItems;

    /**
     * Generates new menu.
     * A list of menu items can be passed as an array.
     * @param array $menuItems
     */
    function __construct(array $menuItems = array())
    {
        $this->menuItems = $menuItems;
    }

    /**
     * Adds a new menu item at the end of a menu
     * @param pixiMenuItem $menuItem
     */
    function addMenuItem(MenuItem $menuItem)
    {
        $this->menuItems[] = $menuItem;
    }

    /**
     * Generates the Bread-Crumb-Navigation for a menu
     * @return string
     */
    function generateBreadCrumb()
    {
        $uriString = uri_string();
        $bread = '';
        foreach ($this->menuItems as $item) {

            if(!empty($item->URL) && strstr($uriString, $item->URL)) {
                $bread .= '<li class="active">'.$item->Text.'</li>';
            }
            
            if (!empty($item->subMenu)) {
                foreach($item->subMenu->menuItems as $subItem) {

                    if(!empty($subItem->URL) && strstr($uriString, $subItem->URL)) {
                        $bread .= '<li class="active">'.$subItem->Text.'</li>';
                    }
                }
            }
        }


        return $bread;
    }

}
