<?php

namespace Pixi\Ui\Menu;

/**
 * Menu item element.
 * 
 * @author pixi
 *        
 */
class MenuItem
{

    /**
     *
     * @var <string> : URL of the menu item.
     */
    public $URL;

    /**
     *
     * @var <string> : Icon of the menu item.
     */
    public $Icon;

    /**
     *
     * @var <strign> : Menu item title.
     */
    public $Text;

    /**
     *
     * @var <Menu> : Menu item submenu.
     */
    public $subMenu;

    /**
     *
     * @var <mixed int> Menu or Submenu item count.
     */
    public $CountSign;
    
    public $target;

    /**
     * Creates new menu item element.
     *
     * @param <string> $URL
     *            : URL of the menu item.
     * @param <string> $Text
     *            : Menu item title.
     * @param <string> $Icon
     *            : Icon of the menu item.
     * @param <Menu> $subMenu
     *            : Menu item submenu.
     * @param <string> $CountSign
     *            : Menu or submenu item count.
     * @throws \Exception
     */
    function __construct($URL, $Text, $Icon = NULL, Menu $subMenu = NULL, $CountSign = NULL, $target = null)
    {
        $this->Text = $Text;
        $this->URL = $URL;
       
        $this->Icon = $Icon;
       
        $this->CountSign = $CountSign;
        
        $this->target = $target;

        if (!empty($subMenu)) {
            $this->setSubMenu($subMenu);
            if (!empty($URL)) {
                throw new \Exception('No URL allowed on Item with Submenu!');
            }
        }
    }

    /**
     * Adding a submenu to a menuitem.
     * 
     * @param pixiMenu $subMenu            
     */
    function setSubMenu(Menu $subMenu)
    {
        $this->subMenu = $subMenu;
    }

    /**
     * returns, if a menu-item is active.
     *
     * This is done by comparing the URL of the menu items to the current open URL
     * TODO: check with more URI elements, like "index.php/feeds/listFeed/123
     * 
     * @return boolean
     */
    function active()
    {
        $uriString = uri_string();
        
        if(empty($uriString)) {
            return false;
        }

        if ((!empty($this->URL)) and (strpos($uriString, $this->URL) !== false OR strpos($this->URL, $uriString) !== false))
            return TRUE;


        if (isset($this->subMenu->menuItems)) {
            foreach ($this->subMenu->menuItems as $subMenuItem) {
                if ($subMenuItem->active())
                    return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * Expands the URL to the full URL
     * 
     * @return string
     */
    function FullURL()
    {
        
        if(strtolower(substr($this->URL, 0, 4)) == 'http') {
            return $this->URL;
        } else {
            return base_url() . $this->URL;
        }
        
    }
    
    public function getTargetOption()
    {
        $target = '';
        if($this->target) {
            $target= ' target="'.$this->target.'" ';
        }
        return $target;
    }

    /**
     * Returns the <A> for an menu item.
     * If no URL is set, it returns the title only.
     * 
     * @return string
     */
    function Anchor()
    {
        
        if (!empty($this->URL)) {
            return '<a href="' . $this->FullURL() . '"' . $this->getTargetOption() . '>' . $this->Text . '</a>';
        } else {
            return $this->Text;
        }
        
    }

}
