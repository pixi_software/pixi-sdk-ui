<?php

namespace Pixi\Ui;

/**
 * These functions will be called to generate Menu, Dropdowns etc.
 * @author pixi
 *
 */
interface UIControllerInterface
{

    /**
     * generateMenu should return a menu object for the main menu
     */
    public function generateMenu();

    /**
     * generateLogDropDown returns the LogDropDown - if empty, no drop down is generated.
     */
    public function generateLogDropDown();

    /**
     * genereateUserDropDown adds additional Items to the "user drop down"
     */
    public function generateUserDropDown();

    /**
     * If you want to add a searchForm to the bread crumb bar, add HTML for it here.
     */
    public function generateSearchForm();

    /**
     * returns the App Title
     */
    public function getAppTitle();
}
