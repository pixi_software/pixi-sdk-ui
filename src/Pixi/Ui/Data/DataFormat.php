<?php

namespace Pixi\Ui\Data;

/**
 * Formating of the data, especially Ui HTML data.
 *
 * @author pixi
 *        
 */
class DataFormat
{

    /**
     *
     * @var <const> : String format type.
     */
    const FORMAT_STRING = 'String';

    /**
     *
     * @var <const> : Money format type.
     */
    const FORMAT_MONEY = 'Money';

    /**
     *
     * @var <const> : Date format type.
     */
    const FORMAT_DATE = 'Date';

    /**
     *
     * @var <const> : Number format type.
     */
    const FORMAT_NUMBER = 'Number';

    /**
     * Performs formatting.
     *
     * @param <mixed> $value
     *            : Value to be formatted.
     * @param <const> $format
     *            : Format type.
     * @param <mixed> $mapping
     *            : Replace values with mapping values. If mapping is array, and array key is
     *            matched with format string type value, the value is replaced with mapping array value.
     * @return string Ambigous unknown>
     */
    public static function format($value, $format, $mapping)
    {
        switch ($format) {

            case self::FORMAT_NUMBER:
                return number_format($value);
                break;

            case self::FORMAT_MONEY:
                if (!is_numeric($value))
                    $value = 0;
                return number_format($value, 2, ',', '.') . config_item('money_currency');
                break;

            case self::FORMAT_DATE:
                return date(config_item('date_format'), strtotime($value));
                break;

            default:
            case self::FORMAT_STRING:

                /**
                 * You can map values if you provide an array.
                 * Array keys are values that are checked in mapping array
                 * as array keys. If they are found the value is replaced with
                 * array value at found key!
                 */
                if (is_array($mapping) && array_key_exists($value, $mapping)) {
                    $value = $mapping[$value];
                }

                return $value;
                break;
        };
    }

}
