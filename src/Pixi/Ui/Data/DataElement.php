<?php

namespace Pixi\Ui\Data;

/**
 * Data element class.
 * 
 * @author gwalter
 *        
 */
abstract class DataElement
{

    /**
     *
     * @var <string> : URL of the data element.
     */
    public $URL;

    /**
     *
     * @var <string> : Title of the data element.
     */
    public $Title;

    /**
     *
     * @var <string> : Subtitle of the data element.
     */
    public $SubTitle;

    /**
     *
     * @var <string> : Icon of the data element.
     */
    public $Icon;

    /**
     * Creates new data element.
     *
     * @param <string> $Title
     *            : Title of the data element.
     * @param <string> $Subtitle
     *            : Subtitle of the data element.
     * @param <string> $Icon
     *            : Icon of the data element.
     * @param <string> $URL
     *            : URL of the data element.
     */
    function __construct($Title, $SubTitle, $Icon = null, $URL = null)
    {
        $this->Title = $Title;
        $this->SubTitle = $SubTitle;
        $this->Icon = $Icon;
        $this->URL = $URL;
    }

}
