<?php

namespace Pixi\Ui\Data;

/**
 * Data container element.
 * 
 * @author gwalter
 *        
 */
abstract class DataContainer
{

    public $Title;
    public $Data;

    /**
     * Constructs new datacontainer.
     * 
     * @param <string> $Title
     *            : Title of the data container.
     */
    function __construct($Title)
    {
        $this->Title = $Title;
    }

    /**
     * Add new element to the data container.
     * 
     * @param DataElement $Element
     *            : Data element.
     */
    function addElement(DataElement $Element)
    {
        $this->Data[] = $Element;
    }

    /**
     * Generates CSV string.
     * 
     * @return string : Returns CSV string.
     */
    function generateCSV()
    {
        if (is_array($this->Data)) {
            $csv = '';
            $rows = (array) $this->Data[0];
            foreach (array_keys($rows) as $row) {
                $csv .= $row . ',';
            }
            $csv .= "\n";
            foreach ($this->Data as $Element) {
                $Element = (array) $Element;
                foreach (array_keys($rows) as $row) {
                    $csv .= $Element[$row] . ',';
                }
            }
        }
        return $csv;
    }

}
