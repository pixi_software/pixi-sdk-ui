<?php

namespace Pixi\Ui\Timeline;

use Pixi\Ui\Data;
use Pixi\Ui\Data\DataElement;

/**
 * Timeline element.
 * 
 * @author gwalter
 *        
 */
class TimelineElement extends DataElement
{

    /**
     *
     * @var <string> : Color of the timeline element.
     */
    public $Color;

    /**
     *
     * @var <const> : Type of the timeline element.
     */
    public $Type;

    /**
     *
     * @var <string> : Time for the timeline element.
     */
    public $Time;

    /**
     *
     * @var <const> : Color of the element for errors.
     */
    const elementTypeError = 'header-color-red2';

    /**
     * Creates new timeline element.
     *
     * @param <string> $Title
     *            : Title of the timeline element.
     * @param <string> $SubTitle
     *            : Subtitle of the timeline element.
     * @param <string> $Time
     *            : Time of the timeline element.
     * @param <const> $Type
     *            : Type of the timeline element.
     * @param <string> $Icon
     *            : Icon for the timeline element.
     * @param <string> $URL
     *            : URL of the timeline element.
     * @param <color> $Color
     *            : Color of the timeline element.
     */
    function __construct($Title, $SubTitle, $Time, $Type = null, $Icon = null, $URL = null, $Color = null)
    {
        parent::__construct($Title, $SubTitle, $Icon, $URL);
        $this->Color = $Color;
        $this->Type = $Type;
        $this->Time = $Time;
    }

}
