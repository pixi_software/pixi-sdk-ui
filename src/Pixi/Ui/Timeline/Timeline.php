<?php

namespace Pixi\Ui\Timeline;

use Pixi\Ui\Data\DataContainer;

/**
 * Timeline ui widget.
 * 
 * @author pixi
 *        
 */
class Timeline extends DataContainer
{

    /**
     * Generates Timeline HTML markup.
     * 
     * @return multitype:string : Returns HTML markup.
     */
    public function generateHTML()
    {
        $html = '
<div class="timeline-container">
	<div class="timeline-label">
		<span class="label label-success arrowed-in-right label-lg"> <b>' . $this->Title . '</b>
		</span>
	</div>
	<div class="timeline-items">
				
				';
        foreach ($this->Data as $Info) {
            $html .= '
<div class="timeline-item clearfix">
			<div class="timeline-info">
				<i
					class="timeline-indicator ace-icon fa ' . $Info->Icon . ' btn btn-' . $Info->Color . ' no-hover "></i>
			</div>

			<div class="widget-box">
				<div class="widget-header ' . $Info->Type . ' widget-header-small">
					<h5 class="smaller">' . $Info->Title . '</h5>
					<span class="widget-toolbar no-border">
						<i class="icon-time bigger-110"></i>
							' . $Info->Time . '
					</span>							
				</div>

				<div class="widget-body">
					<div class="widget-main">
							' . $Info->SubTitle . '
					</div>
				</div>
			</div>
		</div>';
        }

        $html .= '	</div>
					<!-- /.timeline-items -->
					</div>';
        return array(
            'body' => $html
        );
    }

}
