<?php

namespace Pixi\Ui\Dropdown;

class PaymentsDropdown
{
    /**
     * @var Pixi\API\Soap\Client
     */
    private $pixiapi = null;

    /**
     * Result value of the api call which will be used as value in the dropdown.
     * Check the api call documentation for possible values.
     *
     * By default it's 'Code'
     *
     * @access protected
     * @var string
     */
    private $value = 'Code';

    /**
     * Result value of the api call which will be used as option name in the dropdown.
     * Check the api call documentation for possible values.
     *
     * By default it's 'PaymentText'
     *
     * @access protected
     * @var string
     */
    private $optionName = 'PaymentText';

    /**
     * Name of the dropdown element in the browser.
     *
     * By default it's 'paymentTypes'
     *
     * @access protected
     * @var string
     */
    private $name = 'paymentTypes';

    /**
     * Short, Name or XML-Tag of the payment type that
     * should be selected by default when rendering the
     * dropdown.
     *
     * By default it's null
     *
     * @var string
     */
    private $selected;

    /**
     * Sets value, optionName and name of the dropdown with default values
     *
     * @param string $value
     *            Name of the Field in the result set of the api call which will be set as option value
     * @param string $optionName
     *            Name of the Field in the result set of the api call which will be set as option name
     * @param string $name
     *            Name of the dropdown element in the html code.
     * @param string $selected
     *            Short, Name or XML-Tag of the payment type that should be selected by default
     * @return void
     * @throws InvalidArgumentException
     */
    public function __construct($value = null, $optionName = null, $name = null, $selected = null)
    {
        if (!is_string($name)) {
            throw new \InvalidArgumentException("The given parameter '$name' is not a string");
        }

        if (!is_string($selected)) {
            throw new \InvalidArgumentException("The given parameter '$selected' is not a string");
        }

        if (!empty($name)) {
            $this->name = $name;
        }

        if (!empty($selected)) {
            $this->selected = $selected;
        }

        if (!empty($value)) {
            $this->value = $this->validateParam($value);
        }

        if (!empty($optionName)) {
            $this->optionName = $this->validateParam($optionName);
        }

        $this->pixiapi = \pixi_Controller::getAPIClient();
    }

    private function getPaymentTypes()
    {
        return $this->pixiapi->pixiGetPaymenttypes(array())->getResultset();
    }

    /**
     * Renders the payment type dropdown element.
     *
     * @return string
     */
    public function render()
    {
        $output = '<select name="' . $this->name . '">';

        $output .= '<option value="0"> Select payment </option>';

        $paymentTypes = $this->getPaymentTypes();

        foreach ($paymentTypes as $value) {
            $output .= '<option value="' . $value[$this->value] . '" ';
            $output .= $this->getSelected($value) . '>';
            $output .= $value[$this->optionName];
            $output .= '</option>';
        }

        $output .= '</select>';
        return $output;
    }

    public function generateHTML($ID)
    {
        return array(
            'body' => $this->render(),
            'head' => '',
            'script' => ''
        );
    }

    /**
     * Checks if the supplied parameter is actually located in the result set of the api call.
     *
     * @param string $param            
     * @return mixed
     * @throws InvalidArgumentException
     */
    private function validateParam($param)
    {

        $allowed = array(
            'Code',
            'PaymentText',
            'IniOrderStatus',
            '_x0058_MLTag'
        );
        if (in_array($param, $allowed)) {
            return $param;
        }

        throw new \InvalidArgumentException("The given parameter '$param' is not located in the result set of the api call pixiGetPaymenttypes");
    }

    /**
     * Marks an option as selected if the given payment type short, name or xml tag is set.
     *
     * @param array $value            
     * @return string
     */
    private function getSelected($value)
    {
        if (in_array($this->selected, $value)) {
            return 'selected="selected"';
        }
    }

}
