<?php

namespace Pixi\Ui\Log;

/**
 *
 * @author gwalter
 *        
 */
class LogItem
{

    public $log_type;
    public $log_level;
    public $title;
    public $message;
    public $table_name;
    public $table_record_id;

    const LogTypeInfo = 'info';
    const LogTypeWarning = 'warning';
    const LogTypeError = 'error';

    /**
     */
    function __construct($log_type, $log_level, $title, $message = null, $table_name = NULL, $table_record_id = NULL)
    {
        if (($log_type != LogItem::LogTypeError) & ($log_type != LogItem::LogTypeInfo) & ($log_type != LogItem::LogTypeWarning)) {
            //throw new \Exception('Invalid Log Type! Use pixiLogItem::LogType* Constants.');
        };
        $this->log_level = $log_level;
        $this->log_type = $log_type;
        $this->title = $title;
        $this->message = $message;
        $this->table_name = $table_name;
        $this->table_record_id = $table_record_id;
    }

}
