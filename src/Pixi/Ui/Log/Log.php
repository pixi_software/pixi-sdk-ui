<?php

namespace Pixi\Ui\Log;

use Pixi\Ui\Data\DataContainer;
use Pixi\Ui\Timeline\Timeline;
use Pixi\Ui\Timeline\TimelineElement;
use Pixi\Ui\Log\LogItem;

/**
 *
 * @author gwalter
 *        
 */
class Log extends DataContainer
{

    // TODO - Insert your code here

    private static $instance;
    private static $Controller;

    /**
     */
    function __construct($Controller)
    {

        $this->Controller = $Controller;
        /**
          Create Table if not existing...
         */
        $this->Controller->db->simple_query(
                "CREATE TABLE IF NOT EXISTS `logs` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `username` varchar(150) NOT NULL,
                `db` varchar(20) NOT NULL,
                `log_type` varchar(50) NOT NULL,
                `log_level` set('info','warn','error') NOT NULL,
                `title` varchar(200) NOT NULL,				
                `message` longtext  NULL,
                `table_name` varchar(100) DEFAULT NULL,
                `table_record_id` int(11) DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `log_type` (`log_type`)
              ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
    }

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new PixiLog();
        }
        return self::$instance;
    }

    function WriteLogItem(LogItem $LogItem)
    {
        
        if (is_array($LogItem->message)) {
            $LogItem->message = implode($Logitem->message);
        };
        
        $username = strlen($this->Controller->pixiapi->pixi_username) > 0 ? $this->Controller->pixiapi->pixi_username : $this->Controller->app->pixi_username;
        $db = strlen($this->Controller->pixiapi->pixi_db) > 0 ? $this->Controller->pixiapi->pixi_db : $this->Controller->app->pixi_db;
        
        $result = $this->Controller->db->query(
            'INSERT INTO logs (log_type, log_level, title, message, username, db, table_name, table_record_id) VALUES (?, ?, ?, ?, ?, ?, ? ,?)', 
            array(
                $LogItem->log_type, 
                $LogItem->log_level, 
                $LogItem->title, 
                $LogItem->message, 
                $username, 
                $db, 
                $LogItem->table_name, 
                $LogItem->table_record_id       
            )
        );
        
    }

    function readLog()
    {
        $result = $this->Controller->db->query('SELECT * FROM logs ORDER BY TS DESC');
        //var_dump($result->result());
        return $result->result();
    }

    function getLogAsTimeline()
    {
        $result = array();
        $log = $this->readLog();
        $Date = '';
        $LastDate = '';
        if (count($log) > 0) {
            //$timeLine = new pixiTimeline('');
            foreach ($log as $entry) {
                $ts = strtotime($entry->ts);
                $Date = date('d.m.Y', $ts);
                //$LastDate = $Date;
                $Time = date('H:i', $ts);
                // make one timeLine for each Day or date-part
                if ((!isset($tl))) {
                    //echo date('m.Y',strtotime("0 month")).' = '.date('m.Y',$ts).'--------';
                    if ($Date == date('d.m.Y'))
                        $timeLineTitle = 'Heute';
                    elseif ($Date == date('d.m.Y', strtotime("-1 day")))
                        $timeLineTitle = 'Gestern';
                    elseif (date('m.Y', $ts) == date('m.Y', strtotime("0 month")))
                        $timeLineTitle = 'Dieser Monat';
                    elseif (date('m.Y', $ts) == date('m.Y', strtotime("-1 month")))
                        $timeLineTitle = 'Letzter Monat';
                    else
                        $timeLineTitle = 'Älter';
                    $tl = new Timeline($timeLineTitle);
                }
                //new TimelineElement($Title, $SubTitle, $Time, $Icon, $URL, $Color, $Type)
                $icon = '';
                if ($entry->log_type == 'licenses')
                    $icon = 'shopping-cart';
                if ($entry->log_type == 'flexLicenses')
                    $icon = 'fire';
                if ($entry->log_type == 'admin')
                    $icon = 'bug';
                if ($entry->log_type == 'cron')
                    $icon = 'time';
                if ($entry->log_level == LogItem::LogTypeError)
                    $type = TimelineElement::elementTypeError;
                else
                    $type = '';

                $tl->addElement(new TimelineElement($entry->title, $entry->message . ' (DB: ' . $entry->db . ' User: ' . $entry->username . ')', $Date . ' ' . $Time, $icon, null, null, $type));

                if (($Date != $LastDate) & ($LastDate != '')) {
                    $result[] = $tl;
                    $tl = NULL;
                    //echo 'unset me...';
                };
                $LastDate = $Date;
            }
            $result[] = $tl;
        }
        return $result;
    }

}
