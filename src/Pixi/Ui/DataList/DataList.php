<?php

namespace Pixi\Ui\DataList;

/**
 *
 * @author jgololicic
 *        
 */
class DataList
{

    // private $URL;
    private $Title;
    private $Elements = array();
    private $Values;

    /**
     */
    function __construct($Title)
    {
        $this->Title = $Title;
    }

    function addElement($Name, $Format, $Label, $Value = NULL, $mapping = null)
    {
        $this->Elements[] = new DataListElement($Name, $Format, $Label, $Value, $mapping);
    }

    function addValues($Values)
    {
        $this->Values = $Values;
    }

    function generateHTML($ID)
    {
        $html = '<div class="profile-user-info profile-user-info-striped">';

        $counter = 0;
        foreach ($this->Elements as $Element) {

            $html .= '<div class="profile-info-row">
							<div class="profile-info-name" style="width:250px;">' . $Element->Label . '</div>
										<div class="profile-info-value" style="margin-left: 250px;">
											<span class="" id="username-' . $counter . '">';
            $FieldName = $Element->Name;

            if (isset($this->Values[$counter][$FieldName]))
                $value = $this->Values[$counter][$FieldName];
            elseif (isset($this->Values->$FieldName))
                $value = $this->Values->$FieldName;
            elseif (is_string($Element->Value))
                $value = $Element->Value;
            elseif (is_array($this->Values))
                $value = $this->Values[$FieldName];
            else
                $value = ' - ';

            // $html .= '<span id="span-list-'.$counter.'">';
            switch ($Element->Format) {

                case DataListElement::formatNumber:
                    $html .= number_format($value);
                    break;
                case DataListElement::formatMoney:
                    $html .= number_format($value, 2, ',', '.') . ' â‚¬';
                    break;
                case DataListElement::formatString:

                    /**
                     * You can map values if you provide an array.
                     * Array keys are values that are checked in mapping array
                     * as array keys. If they are found the value is replaced with
                     * array value at found key!
                     */
                    if (is_array($Element->mapping) && array_key_exists($value, $Element->mapping)) {
                        $value = $Element->mapping[$value];
                    }

                    $html .= $value;
                    break;

                case DataListElement::formatDate:
                    $html .= date('d.m.Y', strtotime($value));
                    break;

                default:
                    $html .= $value;
                    break;
            }
            ;

            $html .= '</span></div></div>';
            // $html .= '<div class="space-12"></div>';
            $counter ++;
        }

        $html .= '</div>';

        return array(
            'body' => $html,
            'script' => '',
            'head' => ''
        );
    }

    function generateHTML3($ID)
    {
        $html = '<div>';
        // TODO Add to Bitbucket. 
        $counter = 0;
        foreach ($this->Elements as $Element) {
            $counter++;

            $html .= '<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-' . $counter . '">' . $Element->Label . '</label>
										<div class="col-sm-9">';
            $FieldName = $Element->Name;

            if (isset($this->Values->$FieldName))
                $value = $this->Values->$FieldName;
            elseif (is_string($Element->Value))
                $value = $Element->Value;
            elseif (is_array($this->Values))
                $value = $this->Values[$FieldName];
            else
                $value = '';

            $html .= '<span id="span-list-' . $counter . '">';
            switch ($Element->Format) {

                case DataListElement::formatNumber:
                    $html .= number_format($value);
                    break;
                case DataListElement::formatMoney:
                    if ($value != 0)
                        $html .= number_format($value, 2, ',', '.') . ' â‚¬';
                    break;
                case DataListElement::formatString:

                    /**
                     * You can map values if you provide an array.
                     * Array keys are values that are checked in mapping array
                     * as array keys. If they are found the value is replaced with
                     * array value at found key!
                     */
                    if (is_array($column->mapping) && array_key_exists($value, $column->mapping)) {
                        $value = $column->mapping[$value];
                    }

                    $html .= $value;
                    break;

                case DataListElement::formatDate:
                    $html .= date('d.m.Y', strtotime($value));
                    break;

                default:
                    $html .= $value;
                    break;
            }
            ;

            $html .= '</span></div></div>';
        }

        $html .= '</div>';

        return array(
            'body' => $html,
            'script' => '',
            'head' => ''
        );
    }

    /*
     * function generateHTML2($ID) { $html = '<form action="'.site_url($this->URL).'" method="POST" accept-charset="utf-8" class="form-horizontal" role="form" name="form_'.$ID.'">'; $counter = 0; foreach($this->Elements as $Element) { $counter++; //var_dump($this->Values); $FieldName = $Element->Name; if (isset($this->Values->$FieldName)) $value = $this->Values->$FieldName; elseif (is_string($Element->Value)) $value = $Element->Value; elseif (is_array($this->Values)) $value = $this->Values[$FieldName]; else $value = ''; if ($Element->Type == FormElement::ElementTypeCountry) { $Element->Value = array( array('ID' => 'D', 'Name' => lang('germany')), array('ID' => 'A', 'Name' => lang('austria')), array('ID' => 'CH', 'Name' => lang('swiss')), array('ID' => 'F', 'Name' => 'Frankreich'), array('ID' => 'NL', 'Name' => 'Niederlande'), array('ID' => 'CZ', 'Name' => 'Tschechise Rep.'), ); $Element->Type = FormElement::ElementTypeDropDown; } if ($Element->Type != FormElement::ElementTypeID) $html .= '<div class="form-group"> <label class="col-sm-3 control-label no-padding-right" for="form-field-"'.$counter.'>'.$Element->Label.'</label> <div class="col-sm-9">'; switch ($Element->Type) { case FormElement::ElementTypeString: $html .= '<input type="text" id="form-field-'.$counter.'" name="'.$Element->Name.'" class="col-xs-10 col-sm-5" value="'.$value.'">'; break; case FormElement::ElementTypeID: $html .= '<input type="hidden" id="form-field-'.$counter.'" name="'.$Element->Label.'" value="'.$value.'">'; break; case FormElement::ElementTypeHidden: $html .= '<input type="hidden" id="form-field-'.$counter.'" name="'.$Element->Name.'" value="'.$value.'">'; break; case FormElement::ElementTypeNumber: $html .= '<input type="text" id="form-field-'.$counter.'" name="'.$Element->Name.'" class="col-xs-4 col-sm-2" value="'.number_format($value,2,'.','').'">'; break; case FormElement::ElementTypeMoney: $html .= '<input type="text" id="form-field-'.$counter.'" name="'.$Element->Name.'" class="col-xs-4 col-sm-2" value="'.number_format($value,2,'.','').'">'; break; case FormElement::ElementTypeReadOnly: $html .= '<div>'.$value.'</div>'; break; case FormElement::ElementTypeDropDown: //echo 'I am a dropdown - '.$Element->Name; $html .= '<select id="form-field-'.$counter.'" name="'.$Element->Name.'">'; foreach($Element->Value as $ElementValue) { //echo 'ID: '.$ElementValue['ID']; //echo 'Value: '.$value; $html .= '<option value="'.$ElementValue['ID'].'"'; if ($ElementValue['ID'] == trim($value)) $html .= ' SELECTED '; $html .= '>'.$ElementValue['Name'].'</option>'; }; $html .= '</select>'; break; //format }; if ($Element->Type != FormElement::ElementTypeID) $html .= '</div></div>'; } /** This is the submit button for the form... $html .= '<div> <a href="javascript: submitform_'.$ID.'();"	class="btn btn-block btn-primary"> <span>Speichern</span> </a> </div> <script type="text/javascript"> function submitform_'.$ID.'() { document.form_'.$ID.'.submit(); } </script> '; $html .= '</form>'; //$id = $xyz; return array('body' => $html, 'script' => '', 'head' => ''); }
     */
}
