<?php

namespace Pixi\Ui\DataList;

/**
 * Data list element.
 * 
 * @author pixi
 *        
 */
class DataListElement
{

    /**
     *
     * @var <string> : HTML name attribute of the data list element.
     */
    public $Name;

    /**
     *
     * @var <string> : Label of the data list element.
     */
    public $Label;

    /**
     *
     * @var <string> : Value of the data list element.
     */
    public $Value;

    /**
     *
     * @var <const> : Data list element value formatting.
     */
    public $Format;

    /**
     *
     * @param <mixed> $mapping
     *            : If array, the keys are matched agains the $Value and if
     *            found, the array's value is replaced over $Value;
     */
    public $mapping;

    /**
     *
     * @var <const> : String formatting type.
     */
    const formatString = 'string';

    /**
     *
     * @var <const> : Money formatting type.
     */
    const formatMoney = 'money';

    /**
     *
     * @var <const> : Date formatting type.
     */
    const formatDate = 'date';

    /**
     *
     * @var <const> : Number formatting type.
     */
    const formatNumber = 'number';

    /**
     * Creates new data list element.
     *
     * @param <string> $Name
     *            : HTML name property of the data list element.
     * @param <const> $Format
     *            : How to format the value.
     * @param <string> $Label
     *            : Lablel of the data list element.
     * @param <strign> $Value
     *            : Value of the data list element.
     * @param <mixed> $mapping
     *            : If array, the keys are matched agains the $Value and if
     *            found, the array's value is replaced over $Value;
     */
    function __construct($Name, $Format, $Label, $Value = NULL, $mapping)
    {
        $this->Name = $Name;
        $this->Label = $Label;
        $this->Value = $Value;
        $this->Format = $Format;
        $this->mapping = $mapping;
    }

}
