<?php
namespace Pixi\Ui\Chart;

use Pixi\Ui\Table\Table;

class Chart extends Table
{
    
    public $Height = 500;
    public $Width = 600;
    public $NrOfRows = 20;
	public $ChartType = "PieChart";

    public function generateHTML($ID = 0)
    {

        $html['script'] = '<script type="text/javascript" src="https://www.google.com/jsapi"></script>';
        // TODO Move this to the pixiView Controller? So the scripts are only loaded once...
        $html['script'] .= <<<EOD
        <script type="text/javascript">




          // Load the Visualization API and the piechart package.
          google.load('visualization', '1.0', {'packages':['corechart']});

          // Set a callback to run when the Google Visualization API is loaded.
          google.setOnLoadCallback(drawChart);

          // Callback that creates and populates a data table,
          // instantiates the pie chart, passes in the data and
          // draws it.
          function drawChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
EOD;

        $html['body'] = '<h1>' . $this->Title . '</h1>';

        foreach ($this->Columns as $column) {
            $html['script'] .= PHP_EOL . "data.addColumn('" . $column->Format . "', '" . $column->Title . "');";
        }

        $i = 0;
        $chartData = [];
        foreach ($this->Rows as $row) {
            if ($i < $this->NrOfRows) {
                $chartData[] = json_encode(array_values($row));
                $i++;
            } else {
                break;
            }
        }

        $addRows = implode(',', $chartData);

        $html['script'] .= PHP_EOL . 'data.addRows([' . $addRows . ']);';
        $html['script'] .= "

            // Set chart options
            var options = {title:'" . $this->Title . "',
                           width:" . $this->Width . ",
                           height:" . $this->Height . "};

            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.". $this->ChartType ."(document.getElementById('chart_" . $ID . "'));
            chart.draw(data, options);
          }

        </script>";

        $html['body'] = '<div id="chart_' . $ID . '"></div>';

        return $html;
    }

}
