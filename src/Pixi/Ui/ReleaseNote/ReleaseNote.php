<?php

namespace Pixi\Ui\ReleaseNote;

use Pixi\Ui\Data\DataContainer;

/**
 * Info HTML element widget.
 *
 * @author pixi
 */
class ReleaseNote extends DataContainer
{
    /**
     * Generates the HTML markup for the Info widget.
     *
     * @return multitype:string : Returns HTML markup string.
     */
    public function generateHTML()
    {
        $html = '';
        $script = '<script type="text/javascript">';

        foreach ($this->Data as $Item) {

            $html .= '
            <div class="alert alert-block alert-info" id="release_'.$Item->Version.'" hidden >
                <div class="clearfix">
                    <div class="pull-left">
                        <p>
                            <strong><i class="ace-icon fa fa-bullhorn"></i> '.$Item->Title.'</strong><br>'.$Item->SubTitle.'
                        </p>
                    </div>
                </div>
                <p>';
            if ($Item->MoreButton != '')
                $html .= '<a href="'.$Item->URL.'" target="_new"><button class="btn btn-sm btn-success">'.$Item->MoreButton.'</button></a>';
            $html .= '&nbsp;<button class="btn btn-sm" id="hide-'.$Item->Version.'">'.$Item->HideButton.'</button>
                </p>
            </div>';

            $script .= '
            $( document ).ready(function() {
                var hidden = ace.data.get("settings", "release-hidden-' . $Item->Version . '");
              
                if (hidden == 1) {
                    $("#release_' . $Item->Version . '").hide();
                } else {
                    $("#release_' . $Item->Version . '").show();
                }
            
                $("#hide-' . $Item->Version . '").click(function() {
                    ace.data.set("settings", "release-hidden-' . $Item->Version . '", 1);
                    $("#release_' . $Item->Version . '").hide();
                });';

            $script .= '$(\'[data-rel="colorbox"]\').colorbox();';

            $script .= '});';
        }

        $script .= '</script>';

        $script .= '<link rel="stylesheet" href="'. base_url() .'assets/pixi/css/colorbox.css" />';
        $script .= '<script src="'. base_url() .'assets/pixi/js/jquery.colorbox-min.js"></script>';

        return array('body' => $html, 'script' => $script, 'head' => '');
    }
}
