<?php

namespace Pixi\Ui\ReleaseNote;

use Pixi\Ui\Data\DataElement;

/**
 *
 *
 * Info form element.
 *
 * @author pixi
 *
 */
class ReleaseNoteElement extends DataElement
{

    /**
     *
     * @var <string> : Version of the release note.
     */
    public $Version;

    /**
     *
     * @var <string> : Text for More info button.
     */
    public $MoreButton;
    /**
     *
     * @var <string> : Text for Hide box button.
     */
    public $HideButton;

    /**
     *
     * @var <array> : array with list of screenshots .
     * example: array('img/link1.jpg', 'img/link2.jpg')
     */
    public $Screenshots;

    /**
     * Creates new info element.
     *
     * @param <string> $Title
     *            : Title of the info element.
     * @param <string> $SubTitle
     *            : Subtitle of the info element.
     * @param <string> $Icon
     *            : Icon of the info element.
     * @param <string> $Color
     *            : Color of the info element.
     * @param <string> $URL
     *            : URL of the info element.
     */
    function __construct($Title, $SubTitle, $Version, $URL = "", $MoreButton = "", $HideButton = "Nicht mehr anzeigen", $Screenshots = array())
    {
        parent::__construct($Title, $SubTitle, '', $URL, null, null);
        $Version = preg_replace("/[^a-zA-Z0-9]+/", "", $Version); // we only take Numbers and Characters - otherwise our JS doesnt work.
        $this->Version = $Version;
        $this->MoreButton = $MoreButton;
        $this->HideButton = $HideButton;
        $this->Screenshots = $Screenshots;
    }

}
