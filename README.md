Pixi* Ui Library
=======================

Pixi* Ui Library is used for generating Ui objects in the application.
Ui library has multiple objects that lavrage fast creation of Ui elements in your views.

# Ui elements
   * Menu
   * Table
   * Form
   * Dropdown
   * Info
   * Log ...

## ReleaseNote

ReleaseNote is a class to display release notes. They will show in a box, with 2 buttons: more and "don't show again".

<code>
use Pixi\Ui\ReleaseNote\ReleaseNote;
use Pixi\Ui\ReleaseNote\ReleaseNoteElement;

$ReleaseNote = new ReleaseNote('');
//     $ReleaseNote->addElement($Title, $SubTitle, $Version, $URL = "", $MoreButton = "", $HideButton = "Nicht mehr anzeigen")
$ReleaseNote->addElement(new ReleaseNoteElement('Neue Version 1.14', '<ul><li><strong>Artikel nachfüllen</strong> - Sie können die Picklisten App nun auch zum Nachfüllen des Lagerbestands zwischen verschiedenen Locations verwenden. <A href="'.site_url('/refill').'">Funktion aufrufen</a><li><strong>Picken nach Farben</strong> - machen Sie es sich leichter, den richtigen Lagerplatz zu finden, indem Sie eine Farbkodierung für Ihre Lagerplätze einführen. Die Picklisten App markiert die Lagerplatznamen mit der zugehörigen Farbe.</ul>', '1.1', 'https://help.pixi.eu', 'Mehr Infos, Release Notes'));


show the HTML code using $ReleaseNote->generateHTML();

$this->loadMainView('Item Importer', 'Please choose and upload your CSV file. ', array($ReleaseNote,$html));

</code>

# Download API




# Icons
The Template comes with a lot of icons, you can see the list of all available icons here:
http://fontawesome.io/icons/
